#include "questionmodel.h"

#include "questionsinfo.h"

QuestionModel::QuestionModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int QuestionModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!

    return m_questionsList.size();
}

QVariant QuestionModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();


    const auto tmpQuestion = m_questionsList.at(index.row());

    switch (role) {
    case QuestionRole:
        return tmpQuestion->question();

    default:
        return QVariant();
    };

    // FIXME: Implement me!

}

Qt::ItemFlags QuestionModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index); // FIXME: Implement me!
}

QHash<int, QByteArray> QuestionModel::roleNames() const
{
    auto names = QAbstractListModel::roleNames();

    names[QuestionRole] = "question";

    return names;

}

void QuestionModel::updateModel(const QList<QuestionsInfo *> &questions)
{
//    if (m_answers == answers)
//        return;

    beginResetModel();

    m_questionsList = questions;


    endResetModel();
}
