#ifndef ANSWERMODEL_H
#define ANSWERMODEL_H

#include <QAbstractListModel>

class AnswersInfo;

class AnswerModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit AnswerModel(QObject *parent = nullptr);

    enum{
        CheckedRole = Qt::UserRole + 1,
        RightRole,
        AnswerRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    QHash<int, QByteArray> roleNames() const override;

    void updateModel(const QList<AnswersInfo*> &answers = QList<AnswersInfo*>());

private:

    QList<AnswersInfo* >m_answers;
};

#endif // ANSWERMODEL_H
