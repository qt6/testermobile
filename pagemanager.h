#ifndef PAGEMANAGER_H
#define PAGEMANAGER_H

#include <QObject>

class AnswerModel;
class QuestionModel;

class AnswersInfo;
class QuestionsInfo;

class PageManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString question MEMBER m_question NOTIFY questionChanged)
    Q_PROPERTY(QString wrongQuestion MEMBER m_wrongQuestion NOTIFY wrongQuestionChanged)
    Q_PROPERTY(int lostCount MEMBER m_lostCount NOTIFY lostChanged)
    Q_PROPERTY(int rightCount MEMBER m_countRight NOTIFY rightChanged)
    Q_PROPERTY(bool isRandom MEMBER m_isRandom NOTIFY randomChanged)

public:
    explicit PageManager(QObject *parent = nullptr);

    Q_INVOKABLE QuestionModel *questionModel() const;
    Q_INVOKABLE AnswerModel *answerModel() const;
    Q_INVOKABLE AnswerModel *wrongAnswerModel() const;


    Q_INVOKABLE void start();
    Q_INVOKABLE void showRight();


public slots:
    Q_INVOKABLE void  checkAnswer();
    Q_INVOKABLE void  showWrongQuestion(int index);


signals:

    void questionChanged(const QString &newQuestion = QString());
    void wrongQuestionChanged(const QString &newQuestion = QString());
    void lostChanged(int countLost);
    void rightChanged(int countRight);
    void randomChanged(bool isRandom);


private:

    void loadQuestions();

    void  generateQuestion();
    void reset();

    QuestionModel *m_questionModel;
    AnswerModel *m_answerModel;
    AnswerModel *m_wrongAnswerModel;

    QList<QuestionsInfo*> m_wrongQuestionList;
    QList<QuestionsInfo*> m_questionList;
    QList<QuestionsInfo*> m_lostQuestionList;
    QList<AnswersInfo*> m_currentAnswers;
    QList<AnswersInfo*> m_wrongAnswers;
    QuestionsInfo *m_currentQuestion;
    QuestionsInfo *m_currentWrongQuestion;


    QString m_question;
    QString m_wrongQuestion;
    int m_lostCount;
    int m_countRight;

    bool m_isRandom;
};

#endif // PAGEMANAGER_H
