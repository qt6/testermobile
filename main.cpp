#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "pagemanager.h"
#include "answermodel.h"
#include "questionmodel.h"


int main(int argc, char *argv[])
{
    qputenv("QT_QUICK_BACKEND","software");

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    qmlRegisterType<PageManager>("Manager", 1, 0, "PageManager");
    qmlRegisterUncreatableType<AnswerModel>("Answer", 1, 0, "AnswerModel", "That is supported type");
    qmlRegisterUncreatableType<QuestionModel>("Question", 1, 0, "QuestionModel", "That is supported type");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/TesterMobile/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
