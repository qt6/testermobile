import QtQuick 2.15
import QtQuick.Controls 2.15
//import QtQuick.Controls.Basic 2.13
import QtQuick.Layouts 1.13

import Manager 1.0

Window {

    visible: true

    width: 640
    height: 360

    PageManager {
        id: pgManager
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        Page {
            id: mainPage
            property int pageIndex: SwipeView.index
            ColumnLayout {
                anchors.centerIn: parent
                Button {
                    id: marathonBtn
                    Layout.alignment: Qt.AlignHCenter
                    text: "Марафон"
                    onClicked: {
                        swipeView.currentIndex = questionPage.pageIndex
                        questionPage.manager.start()
                    }
                }
                Button {
                    id: wrongBtn
                    Layout.alignment: Qt.AlignHCenter
                    text: "К ошибкам"
                    onClicked: {
                        swipeView.currentIndex = wrongPage.pageIndex
                    }
                }
                CheckBox {
                    Layout.alignment: Qt.AlignHCenter
                    text: "Вопросы и ответы в случайном порядке"
                    checked: questionPage.manager.isRandom
                    onToggled: questionPage.manager.isRandom = checked
                }
            }
        }

        QuestionPage {
            id: questionPage
            manager: pgManager
            pageIndex: SwipeView.index
        }

        WrongPage {
            id: wrongPage
            manager: pgManager
            pageIndex: SwipeView.index
            onRequestReturn: swipeView.currentIndex = mainPage.pageIndex
            onItemClicked: swipeView.currentIndex = wrongQuestionPage.pageIndex
        }

        WrongQuestionPage {
            id: wrongQuestionPage
            manager: pgManager
            pageIndex: SwipeView.index
        }
    }
}
