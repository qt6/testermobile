import QtQuick 2.15
import QtQuick.Controls 2.15
//import QtQuick.Controls.Basic 2.13
import QtQuick.Layouts 1.13

Page {
    id: root

    required property var manager
    required property int pageIndex

    header: Text {
        id: questionBlock
        Layout.fillHeight: false
        Layout.fillWidth: true
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 20
        text: manager.wrongQuestion
    }

    ListView {

        id: listView
        anchors.fill: parent

        model: manager.wrongAnswerModel()
        clip: true
        delegate: CheckDelegate {
            id: control
            text: model.answer
            font.pixelSize: 14

            width: listView.width

            LayoutMirroring.enabled: true

            contentItem: Text {
                leftPadding: control.mirrored ? control.indicator.width + control.spacing : 0
                rightPadding: !control.mirrored ? control.indicator.width + control.spacing : 0
                text: control.text
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignJustify
                verticalAlignment: Text.AlignVCenter
                font: control.font
                color: (model.right) ? "green" : (model.check
                                                  && !model.right) ? "red" : "black"
            }

            checked: model.check
            checkable: false
        }
    }
}
