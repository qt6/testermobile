#ifndef QUESTIONMODEL_H
#define QUESTIONMODEL_H

#include <QAbstractListModel>

class QuestionsInfo;

class QuestionModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit QuestionModel(QObject *parent = nullptr);

    enum{
        QuestionRole = Qt::UserRole + 1
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    QHash<int, QByteArray> roleNames() const override;

    void updateModel(const QList<QuestionsInfo*> &questions = QList<QuestionsInfo*>() );

private:

    QList<QuestionsInfo* >m_questionsList;
};

#endif // QUESTIONMODEL_H
