import QtQuick 2.15
import QtQuick.Controls 2.15
//import QtQuick.Controls.Basic 2.13
import QtQuick.Layouts 1.13

Page {
    id: root

    required property var manager

    property bool showRights: false
    required property int pageIndex

    header: Text {
        id: questionBlock
        Layout.fillHeight: false
        Layout.fillWidth: true
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 20
        text: manager.question
        MouseArea {
            anchors.fill: parent
            onClicked: manager.showRight()
        }
    }

    ListView {

        id: listView
        anchors.fill: parent

        model: manager ? manager.answerModel() : null
        clip: true
        delegate: CheckDelegate {
            id: control
            text: model.answer
            font.pixelSize: 14

            width: listView.width

            LayoutMirroring.enabled: true

            contentItem: Text {
                leftPadding: control.mirrored ? control.indicator.width + control.spacing : 0
                rightPadding: !control.mirrored ? control.indicator.width + control.spacing : 0
                text: control.text
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignJustify
                verticalAlignment: Text.AlignVCenter
                font: control.font
            }

            checked: model.check
            onClicked: model.check = checked
        }
    }

    footer: RowLayout {
        Item {
            implicitHeight: lostText.implicitHeight
            Layout.fillWidth: true
            Layout.fillHeight: true
            Text {
                id: lostText
                text: manager.lostCount
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
            }
        }

        Item {
            implicitHeight: checkBtn.implicitHeight
            Layout.fillWidth: true
            Layout.fillHeight: true
            Button {
                id: checkBtn
                text: "Ответить"
                enabled: manager.lostCount
                anchors.centerIn: parent
                onClicked: manager.checkAnswer()
            }
        }

        Item {
            implicitHeight: rightText.implicitHeight
            Layout.fillWidth: true
            Layout.fillHeight: true
            Text {
                id: rightText
                text: manager.rightCount
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                visible: false
            }
            MouseArea {
                anchors.fill: parent
                onClicked: rightText.visible = !rightText.visible
            }
        }
    }
}
