#include "pagemanager.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QRandomGenerator>

#include "questionsinfo.h"
#include "questionmodel.h"
#include "answermodel.h"
#include "answersinfo.h"

PageManager::PageManager(QObject *parent)
    : QObject{parent}
{
    loadQuestions();

    m_answerModel = new AnswerModel(this);
    m_wrongAnswerModel = new AnswerModel(this);
    m_questionModel = new QuestionModel(this);

    m_isRandom = false;

    reset();
}

QuestionModel *PageManager::questionModel() const
{
    return m_questionModel;
}

AnswerModel *PageManager::answerModel() const
{
    return m_answerModel;
}

AnswerModel *PageManager::wrongAnswerModel() const
{
    return m_wrongAnswerModel;
}

void PageManager::start()
{
    reset();
    generateQuestion();
}

void PageManager::showRight()
{
    if(!m_currentQuestion)
        return;

    for(auto answer:m_currentAnswers)
    {
        answer->setIsChecked(answer->isRight());
    }

    m_answerModel->updateModel(m_currentAnswers);
}

void PageManager::generateQuestion()
{
    //    int randIndex=;
    int index = m_isRandom ? QRandomGenerator::global()->bounded(m_lostQuestionList.size()) : 0;

    if(auto tmpQuest = m_lostQuestionList.takeAt(index))
    {
        m_currentQuestion = tmpQuest;
        emit questionChanged(m_question = tmpQuest->question());

        if(m_isRandom)
            m_currentAnswers=tmpQuest->randomAnswers();
        else
            m_currentAnswers=tmpQuest->answers();

        m_answerModel->updateModel(m_currentAnswers);
    }
}

void PageManager::checkAnswer()
{

    if(!m_currentQuestion)
        return;

    int tmpCount = 0;

    auto tmpAnswerList=m_currentQuestion->answers();

    for(const auto *answer:tmpAnswerList)
    {
        tmpCount += (answer->isChecked()==answer->isRight());
    }

    if(tmpCount != tmpAnswerList.size()){
        m_wrongQuestionList.append(m_currentQuestion);
        m_questionModel->updateModel(m_wrongQuestionList);
    }

    emit rightChanged(m_countRight += (tmpCount == tmpAnswerList.size()));


    emit lostChanged(m_lostCount=m_lostQuestionList.size());


    if(m_lostCount)
        generateQuestion();

}

void PageManager::showWrongQuestion(int index)
{
    if(index<0 || index>m_wrongQuestionList.size())
        return;

    if(auto tmpWronQuestion=m_wrongQuestionList.at(index)){

        m_currentWrongQuestion = tmpWronQuestion;

        emit wrongQuestionChanged(m_wrongQuestion = m_currentWrongQuestion->question());
        m_wrongAnswers = tmpWronQuestion->answers();

        m_wrongAnswerModel->updateModel(m_wrongAnswers);

    }
}

void PageManager::loadQuestions()
{

    QList<QuestionsInfo *> tmpList;

    QFile loadFile(":/files/questionFile");

    if(!loadFile.open(QFile::ReadOnly))
        return;

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));


    auto questArr = loadDoc.object()["Questions"].toArray();

    tmpList.clear();


    for(int index=0; index < questArr.size(); ++index){
        QuestionsInfo *quest=new QuestionsInfo;

        quest->read(questArr.at(index).toObject());

        tmpList.append(quest);
    }

    m_questionList = tmpList;
}

void PageManager::reset()
{

    for(auto question : m_questionList)
        for(auto answer : question->answers())
            answer->setIsChecked(false);

    m_lostQuestionList = m_questionList;
    m_wrongQuestionList.clear();

    m_currentQuestion = nullptr;
    m_currentWrongQuestion = nullptr;
    m_question.clear();
    m_wrongQuestion.clear();

    emit lostChanged(m_lostCount = m_lostQuestionList.size());
    emit rightChanged(m_countRight = 0);
    emit questionChanged();
    emit wrongQuestionChanged();

    m_questionModel->updateModel();
    m_wrongAnswerModel->updateModel();

}
