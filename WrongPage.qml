import QtQuick 2.15
import QtQuick.Controls 2.15
//import QtQuick.Controls.Basic 2.13
import QtQuick.Layouts 1.13

import Manager 1.0

Page {
    id: root

    required property var manager
    required property int pageIndex

    signal itemClicked
    signal requestReturn

    ListView {

        id: listView
        anchors.fill: parent

        model: manager.questionModel()
        clip: true
        delegate: ItemDelegate {
            id: control
            text: model.question

            //            font.pixelSize: 14
            width: listView.width

            contentItem: Text {
                leftPadding: control.mirrored ? control.spacing : 0
                rightPadding: !control.mirrored ? control.spacing : 0
                text: control.text
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignJustify
                verticalAlignment: Text.AlignVCenter
                font: control.font
            }
            onClicked: {
                root.itemClicked()
                manager.showWrongQuestion(model.index)
            }
        }
    }
    footer: Button {
        text: "В начало"
        onClicked: root.requestReturn()
    }
}
