include(../common.pri)

VERSION = 1.0.0.1

QT += quick

TARGET = TesterMobile

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    answermodel.h \
    pagemanager.h \
    questionmodel.h

SOURCES += main.cpp \
    answermodel.cpp \
    pagemanager.cpp \
    questionmodel.cpp

qml.files = main.qml \
            QuestionPage.qml \
            WrongPage.qml \
            WrongQuestionPage.qml

qml.prefix = /$${TARGET}
RESOURCES += qml


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =


contains(ANDROID_TARGET_ARCH,arm64-v8a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

