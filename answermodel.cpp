#include "answermodel.h"

#include "answersinfo.h"

AnswerModel::AnswerModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int AnswerModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!

    return m_answers.size();
}

QVariant AnswerModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();


    const auto tmpAnswer = m_answers.at(index.row());

    switch (role) {
    case CheckedRole:
        return tmpAnswer->isChecked();
    case RightRole:
        return tmpAnswer->isRight();
    case AnswerRole:
        return tmpAnswer->answer();
    default:
        return QVariant();
    };

    // FIXME: Implement me!

}

bool AnswerModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    const auto tmpAnswer = m_answers.at(index.row());

    switch (role) {
        case CheckedRole:
        tmpAnswer->setIsChecked(value.toBool());
        break;
    default:
        break;
    };


    if (data(index, role) != value) {
        // FIXME: Implement me!
        emit dataChanged(index, index, {role});
        return true;
    }

    return false;
}

Qt::ItemFlags AnswerModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable; // FIXME: Implement me!
}

QHash<int, QByteArray> AnswerModel::roleNames() const
{
    auto names = QAbstractListModel::roleNames();

    names[CheckedRole] = "check";
    names[RightRole] = "right";
    names[AnswerRole] = "answer";

    return names;

}

void AnswerModel::updateModel(const QList<AnswersInfo *> &answers)
{
//    if (m_answers == answers)
//        return;

    beginResetModel();

    m_answers = answers;


    endResetModel();
}
